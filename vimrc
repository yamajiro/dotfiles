
"neobundle settings
set runtimepath+=~/.vim/bundle/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.vim/bundle')

  call dein#begin('~/.vim/bundle')
  call dein#add('~/.vim/bundle')

  call dein#add('tpope/vim-fugitive')
  call dein#add('idanarye/vim-merginal')
  call dein#add('cohama/agit.vim')
  call dein#add('embear/vim-localvimrc')

  if has('job') && has('channel') && has('timers')
    call dein#add('w0rp/ale')
    call dein#add('itchyny/lightline.vim')
    call dein#add('maximbaz/lightline-ale')


    call dein#add('Shougo/denite.nvim')
    call dein#add('Shougo/deoplete.nvim')
    call dein#add('deoplete-plugins/deoplete-clang')
    call dein#add('deoplete-plugins/deoplete-jedi')
    call dein#add('sebastianmarkow/deoplete-rust')
    if has('python3') && !has('nvim')
      call dein#add('roxma/nvim-yarp')
      call dein#add('roxma/vim-hug-neovim-rpc')
    endif
  else
    call dein#add('Shougo/unite.vim')
    call dein#add('scrooloose/syntastic')
  endif

  call dein#end()
  call dein#save_state()
endif

if dein#check_install()
  call dein#install()
endif

set statusline+=%{fugitive#statusline()}

if dein#tap('ale')
  let g:ale_linters = {
  \ 'cpp': ['gcc', 'clang'],
  \ 'c': ['gcc',' clang'],
  \ 'lua': ['luac'],
  \ 'python': ['flake8', 'autopep8', 'pyflakes'],
  \ 'rust': ['analyzer', 'rls'],
  \ }
  let g:ale_statusline_format = ['E%d', 'W%d', 'OK']
  let g:ale_sign_column_always = 1
  let g:ale_echo_msg_error_str = 'E'
  let g:ale_echo_msg_warning_str = 'W'
  let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
endif

if dein#tap('lightline.vim')
    let g:lightline = {
    \ 'colorscheme': 'wombat',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'readonly', 'filename', 'modified' ],
    \             [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok' ] ],
    \ },
    \ }
endif

if dein#tap('lightline-ale')
  let g:lightline.component_expand = {
        \  'linter_checking': 'lightline#ale#checking',
        \  'linter_warnings': 'lightline#ale#warnings',
        \  'linter_errors': 'lightline#ale#errors',
        \  'linter_ok': 'lightline#ale#ok',
        \ }
  let g:lightline.component_type = {
        \     'linter_checking': 'left',
        \     'linter_warnings': 'warning',
        \     'linter_errors': 'error',
        \     'linter_ok': 'left',
        \ }
endif

if dein#tap('syntastic')
  set statusline+=%#warningmsg#
  set statusline+=%{SyntasticStatuslineFlag()}
  set statusline+=%*

  let g:syntastic_always_populate_loc_list = 1
  let g:syntastic_auto_loc_list = 1
  let g:syntastic_enable_signs = 1
  let g:syntastic_check_on_open = 1
  let g:syntastic_check_on_wq = 0

  let g:syntastic_cpp_checkers = ['gcc','clang']
  let g:syntastic_cpp_compiler_options = ' -std=c++17'
  let g:syntastic_cpp_check_header = 1
  let g:syntastic_cpp_no_include_search = 1

  let g:syntastic_enable_perl_checker = 1
  let g:syntastic_perl_checkers = ['perl','podchecker']
endif

if dein#tap('deoplete.nvim')
  let g:deoplete#enable_at_startup = 1
  call deoplete#custom#source('_','min_pattern_length',4)
endif

if dein#tap('vim-localvimrc')
  let g:localvimrc_sandbox = 0
  let g:localvimrc_ask = 0
endif

set autoindent
set number
set showmode
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=0
set encoding=utf-8
set fileencoding=utf-8
set laststatus=2
colorscheme desert
syntax enable


