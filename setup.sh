#!/bin/sh

ln -sf ${HOME}/.dotfiles/vimrc ~/.vimrc
ln -sf ${HOME}/.dotfiles/gitconfig ~/.gitconfig

if [ ! -e ~/.vim/bundle ] ;then
	mkdir -p ~/.vim/bundle
fi

curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > ~/.vim/bundle/installer.sh 
sh ~/.vim/bundle/installer.sh ~/.vim/bundle
rm ~/.vim/bundle/installer.sh
